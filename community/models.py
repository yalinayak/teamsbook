#-*- coding: utf-8 -*-

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _

from utilities.models import Image


class Team(models.Model):
    name = models.CharField(
        verbose_name=_(u'Takım adı'),
        help_text=_(u'Takımın adnını belirtir.'),
        max_length=40,
        unique=True)
    photo = models.ImageField(
        _(u'Takım Profil Fotoğrafı'),
        help_text=_(u'Takım profil fotoğrafı'),
        upload_to='team_profile_photos/',
        null=True,
        blank=True)
    photos = models.ForeignKey(
        Image,
        blank=True,
        null=True,
        help_text=_(u'Fotoğraflar'),
        verbose_name=_(u'Fotoğraflar'))

    class Meta:
        verbose_name = _(u'takım')
        verbose_name_plural = _(u'takımlar')


class Group(models.Model):
    name = models.CharField(
        verbose_name=_(u'Grup adı'),
        help_text=_(u'Grubunuzun adnını belirtir.'),
        max_length=40,
        unique=True)
    admin = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u'Yöneticiler'),
        help_text=_(u'Grubun yöneticilerini belirtir'))
    photo = models.ImageField(
        _(u'Grup Profil Fotoğrafı'),
        help_text=_(u'Grup profil fotoğrafı'),
        upload_to='group_profile_photos/',
        null=True,
        blank=True)
    photos = models.ForeignKey(
        Image,
        blank=True,
        null=True,
        help_text=_(u'Fotoğraflar'),
        verbose_name=_(u'Fotoğraflar'))

    class Meta:
        verbose_name = _(u'grup')
        verbose_name_plural = _(u'gruplar')
