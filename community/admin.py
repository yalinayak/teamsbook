from django.contrib import admin

from community.models import Group, Team


admin.site.register(Group)
admin.site.register(Team)
