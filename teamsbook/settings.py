try:
    from settings_local import *
except ImportError:
    try:
        from settings_server import *
    except ImportError:
        from settings_default import *
