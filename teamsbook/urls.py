#-*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'feed.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^hesap/', include('account.urls')),
    url(r'^yorumlar/', include('django.contrib.comments.urls')),
    url(r'^mesajlar/', include('communication.urls')),
)

urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
    'document_root': settings.MEDIA_ROOT}))
