#-*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from account.models import CustomUser


class Conversation(models.Model):
    inbox = models.ForeignKey(CustomUser, related_name='inbox')
    with_user = models.ForeignKey(CustomUser)
    last_message_time = models.DateTimeField(auto_now=True)


class Message(models.Model):
    conversation = models.ForeignKey(Conversation, related_name='messages')
    is_sent = models.BooleanField()
    date = models.DateTimeField(auto_now_add=True)
    message = models.CharField(
        max_length=140,
        verbose_name=_(u'mesaj'),
        help_text=_(u'Mesajınız 140 karakteri geçmemeli'),
        null=True,
        blank=True)

    class Meta:
        verbose_name = _(u'mesaj')
        verbose_name_plural = _(u'mesajlar')
