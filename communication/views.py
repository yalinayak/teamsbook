from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render

from account.models import CustomUser
from communication.forms import MessageForm
from communication.models import Conversation, Message


@login_required
def inbox(request):
    user = request.user
    # users conversations
    inbox = Conversation.objects.filter(inbox=user)

    conversations = []
    for conversation in inbox:
        messages = conversation.messages.all().order_by('-date')
        if messages:
            message_content = messages[0].message
        else:
            continue
        conversations.append({'last_message': message_content, 'sender': conversation.with_user})

    return render(request, 'communication/inbox.html', {'conversations': conversations})


@login_required
def conversation(request, user_id):
    conversation_with = get_object_or_404(CustomUser, id=user_id)

    conversation_in, is_created = Conversation.objects.get_or_create(inbox=request.user, with_user=conversation_with)
    conversation = Message.objects.filter(conversation=conversation_in).order_by('date')
    conversation_out, is_created = Conversation.objects.get_or_create(inbox=conversation_with, with_user=request.user)

    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            message = form.save(commit=False)
            message.conversation = conversation_in
            message.is_sent = True
            message.save()

            Message.objects.create(message=form.cleaned_data["message"],
                                   conversation=conversation_out,
                                   is_sent=False)

            conversation = Message.objects.filter(conversation=conversation_in).order_by('date')

            form = MessageForm()
            return render(request, 'communication/conversation.html', {'conversation': conversation,
                                                                       'message_form': form,
                                                                       'conversation_user': conversation_with})
        else:
            return render(request, 'communication/conversation.html', {'conversation': conversation,
                                                                       'message_form': form,
                                                                       'conversation_user': conversation_with})
    else:
        form = MessageForm()
        return render(request, 'communication/conversation.html', {'conversation': conversation,
                                                                   'message_form': form,
                                                                   'conversation_user': conversation_with})
