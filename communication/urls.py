from django.conf.urls import patterns
from django.conf.urls import url

from communication.views import inbox, conversation


urlpatterns = patterns('',
                       url(r'^mesajlar/(?P<user_id>\d+)/',
                           conversation,
                           name='conversation'),
                       url(r'^mesajlar/',
                           inbox,
                           name='inbox'),
                       )
