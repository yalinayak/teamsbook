from django import forms

from communication.models import Message


class MessageForm(forms.ModelForm):
    message = forms.CharField( widget=forms.Textarea )
    class Meta:
        model = Message
        fields = ('message',)
