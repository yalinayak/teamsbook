#-*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser,
                                        PermissionsMixin, UserManager)
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext as _
from django.utils import timezone

from account.teams import TEAM_CHOICES
from community.models import Group


class CustomUserManager(BaseUserManager):
    def create_user(self, username, name, last_name, date_of_birth, gender,
                    team, password=None,  email=None, **extra_fields):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError(_('Kullanıcının mutlaka e-posta adresi olmalıdır.'))
        user = self.model(
            email=UserManager.normalize_email(email),
            username=username,
            name=name, last_name=last_name,
            date_of_birth=date_of_birth, gender=gender, team=team,
            is_staff=False, is_active=True, is_superuser=False,
            last_login=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """

        name = 'Yalınayak'
        last_name = 'The Developer'
        date_of_birth = '1970-01-01'
        team = 1
        gender = "M"
        user = self.create_user(username, name, last_name,
                                date_of_birth, gender, team, password, email)
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    # Every user has a unique username.

    MALE = "M"
    FEMALE = "F"

    GENDER_CHOICES = (
        (MALE, _(u"Erkek")),
        (FEMALE, _(u"Kadın")))

    username = models.CharField(
        verbose_name=_(u'kullanıcı adı'),
        max_length=40,
        unique=True)
    name = models.CharField(
        verbose_name=_(u'ad'),
        max_length=20)
    last_name = models.CharField(
        verbose_name=_(u'soyad'),
        max_length=40)
    date_of_birth = models.DateField(
        verbose_name=_(u'doğum tarihi'))
    gender = models.CharField(
        verbose_name=_(u"Cinsiyet"),
        max_length=1,
        choices=GENDER_CHOICES)
    team = models.IntegerField(
        verbose_name=_(u'Takım'),
        help_text=_(u'Tuttuğunuz takımı belirtir.'),
        max_length=4,
        choices=TEAM_CHOICES)
    is_active = models.BooleanField(
        verbose_name=_(u'aktif mi?'),
        help_text=_(u'Kullanıcının hesabının aktif olup olmadığını belirtir.'),
        default=True)
    is_staff = models.BooleanField(
        verbose_name=_(u'yönetici mi?'),
        help_text=_(u"""Kullanıcının sitede yönetici haklarına
           sahip olup olmadığını belirtir."""),
        default=False)
    date_joined = models.DateTimeField(
        (u'katılım tarihi'),
        default=timezone.now)
    # Every user has a unique email which is also used for identification.
    email = models.EmailField(
        verbose_name=_(u'e-posta adresi'),
        max_length=254,
        unique=True,
        db_index=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'name', 'last_name',
                       'date_of_birth', 'gender']

    objects = CustomUserManager()

    photo = models.ImageField(
        _(u'Profil Fotoğrafı'),
        upload_to='profile_photos/',
        null=True,
        blank=True)
    friends = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True)
    fan_groups = models.ManyToManyField(
        Group,
        related_name='groups',
        blank=True,
        null=True)

    class Meta:
        verbose_name = _(u'kullanıcı')
        verbose_name_plural = _(u'kullanıcılar')

    def get_full_name(self):
        # The user is identified by their full name.
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        # The user is identified by their username
        return self.username

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def __unicode__(self):
        return self.email
