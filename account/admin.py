from django.contrib import admin
# from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext as _

from account.models import CustomUser
from account.forms import UserChangeForm, UserCreationForm


class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('username', 'email', 'name', 'last_name', 'date_of_birth',
                    'gender', 'team', 'photo', 'is_staff', 'is_superuser')
    list_filter = ('is_staff',)
    fieldsets = (
        (None, {'fields': ('email', 'username', 'password')}),
        (_("Personal info"), {'fields': ('name', 'last_name', 'date_of_birth',
                              'gender', 'team', 'photo')}),
        (_("Permissions"), {'fields': ('is_staff', 'is_superuser')}),
        (_("Important dates"), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'name', 'last_name',
            'date_of_birth', 'gender', 'team', 'password1', 'password2')}),
    )
    search_fields = ('email', 'username', 'name', 'last_name')
    ordering = ('email', 'username')
    filter_horizontal = ()

admin.site.register(CustomUser, CustomUserAdmin)
