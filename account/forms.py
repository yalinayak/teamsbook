#-*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.translation import ugettext as _

from account.models import CustomUser


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label=_(u'Parola'), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_(u'Parola Doğrulama'), widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('email', 'name', 'last_name', 'gender',
                  'date_of_birth', 'team')

    def clean_username(self):
        """
        Validate that the username is not already in use.

        """
        existing = CustomUser.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("Bu isimli bir kullanıcı var."))
        else:
            return self.cleaned_data['username']

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_(u"Parolanız eşleşmiyor!"))
        return password2

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        if CustomUser.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("Bu e-mail adresi kullanımda. Başka bir e-mail adresi girin ve ya şifremi unuttum seçeneğini kullanın"))
        return self.cleaned_data['email']

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]
