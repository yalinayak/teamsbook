from django.conf.urls import patterns, url
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView

from account.views import profile

from registration.backends.default.views import ActivationView
from registration.backends.default.views import RegistrationView

urlpatterns = patterns('',
                       url(r'^profil/(?P<user_id>\d+)/',
                           profile,
                           name='profile'),
                       url(r'^profil/',
                           profile,
                           name='my_profile'),

                       # Registration
                       url(r'^aktivasyon/tamamlandi/$',
                           TemplateView.as_view(template_name='registration/activation_complete.html'),
                           name='registration_activation_complete'),
                       # Activation keys get matched by \w+ instead of the more specific
                       # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
                       # that way it can return a sensible "invalid key" message instead of a
                       # confusing 404.
                       url(r'^aktivasyon/(?P<activation_key>\w+)/$',
                           ActivationView.as_view(),
                           name='registration_activate'),
                       url(r'^kayit/$',
                           RegistrationView.as_view(),
                           name='registration_register'),
                       url(r'^kayit/tamamlandi/$',
                           TemplateView.as_view(template_name='registration/registration_complete.html'),
                           name='registration_complete'),
                       url(r'^kayit/kapandi/$',
                           TemplateView.as_view(template_name='registration/registration_closed.html'),
                           name='registration_disallowed'),

                       # Authentication
                       url(r'^giris/$',
                           auth_views.login,
                           {'template_name': 'account/login.html'},
                           name='login'),
                       url(r'^cikis/$',
                           auth_views.logout,
                           {'template_name': 'account/logout.html'},
                           name='logout'),

                       # Password Change
                       url(r'^sifre/degistir/$',
                           auth_views.password_change,
                           name='auth_password_change'),
                       url(r'^sifre/degistir/tamam/$',
                           auth_views.password_change_done,
                           name='auth_password_change_done'),

                       # Password Reset
                       url(r'^sifre/yenile/$',
                           auth_views.password_reset,
                           name='auth_password_reset'),
                       url(r'^sifre/yenile/onayla/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
                           auth_views.password_reset_confirm,
                           name='auth_password_reset_confirm'),
                       url(r'^sifre/yenile/tamamlandi/$',
                           auth_views.password_reset_complete,
                           name='auth_password_reset_complete'),
                       url(r'^sifre/yenile/tamam/$',
                           auth_views.password_reset_done,
                           name='auth_password_reset_done'),
                       )
