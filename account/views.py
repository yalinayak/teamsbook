from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render

from account.models import CustomUser
from account.forms import UserCreationForm
from feed.forms import PostForm, TextFeedForm, PhotoFeedForm, LinkFeedForm
from feed.models import UserPost

from registration.backends.default.views import RegistrationView


@login_required
def profile(request, user_id=None):
    if user_id:
        profile = get_object_or_404(CustomUser, id=user_id)
        profile_feeds = UserPost.objects.filter(owner=profile).order_by('-date')
        return render(request, 'account/profile.html', {'profile': profile,
                                                        'profile_feeds': profile_feeds})
    else:
        profile = request.user
        if request.method == "POST":
            form = PostForm(request.POST, request.FILES)
            if form.is_valid():
                post = form.save(commit=False)
                post.owner = request.user
                post.save()
                profile_feeds = UserPost.objects.filter(owner=profile).order_by('-date')
                message_form = TextFeedForm()
                photo_message_form = PhotoFeedForm()
                link_message_form = LinkFeedForm()
                return render(request, 'account/profile.html', {'profile': profile,
                                                                'profile_feeds': profile_feeds,
                                                                'message_form': message_form,
                                                                'photo_message_form': photo_message_form,
                                                                'link_message_form': link_message_form})
            else:
                profile_feeds = UserPost.objects.filter(owner=profile).order_by('-date')
                message_form = TextFeedForm(request.POST)
                photo_message_form = PhotoFeedForm(request.POST, request.FILES)
                link_message_form = LinkFeedForm(request.POST)
                return render(request, 'account/profile.html', {'profile': profile,
                                                                'profile_feeds': profile_feeds,
                                                                'message_form': message_form,
                                                                'photo_message_form': photo_message_form,
                                                                'link_message_form': link_message_form})
        else:
            profile_feeds = UserPost.objects.filter(owner=profile).order_by('-date')
            message_form = TextFeedForm()
            photo_message_form = PhotoFeedForm()
            link_message_form = LinkFeedForm()
            return render(request, 'account/profile.html', {'profile': profile,
                                                            'profile_feeds': profile_feeds,
                                                            'message_form': message_form,
                                                            'photo_message_form': photo_message_form,
                                                            'link_message_form': link_message_form})


class CustomRegistrationView(RegistrationView):
    """
    Custom registration view that uses our custom form.
    """
    form_class = UserCreationForm
