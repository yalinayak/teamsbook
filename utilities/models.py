#-*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _


class Image(models.Model):
    image = models.ImageField(
        _(u'Fotoğraf'),
        upload_to='photos/')
    description = models.CharField(
        max_length=140,
        null=True,
        blank=True,
        verbose_name=_(u'Açıklama'))
    # tags ?
    # comments

    class Meta:
        verbose_name = _(u'fotğraf')
        verbose_name_plural = _(u'fotoğraflar')
