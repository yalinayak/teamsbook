from django import forms
from django.utils.translation import ugettext as _

from feed.models import UserPost


class PostForm(forms.ModelForm):
    message = forms.CharField(label=_(u'Mesaj'), widget=forms.Textarea)
    class Meta:
        model = UserPost
        fields = ('message', 'image', 'url')


class TextFeedForm(forms.ModelForm):
    message = forms.CharField(label=_(u'Mesaj'), widget=forms.Textarea)
    class Meta:
        model = UserPost
        fields = ('message',)

class PhotoFeedForm(forms.ModelForm):
    message = forms.CharField(label=_(u'Mesaj'), widget=forms.Textarea)
    class Meta:
        model = UserPost
        fields = ('message', 'image')

class LinkFeedForm(forms.ModelForm):
    message = forms.CharField(label=_(u'Mesaj'), widget=forms.Textarea)
    class Meta:
        model = UserPost
        fields = ('message', 'url')
