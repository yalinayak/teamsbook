#-*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from account.models import CustomUser
from community.models import Group
# from utilities.models import Image


class Post(models.Model):
    url = models.URLField(
        _(u'Link'),
        blank=True,
        null=True)
    message = models.CharField(
        _(u'Mesaj'),
        max_length=140,
        blank=True,
        null=True)
    """
    image = models.OneToOneField(
        Image,
        verbose_name=_(u'Fotoğraf'),
        blank=True,
        null=True)
    """
    image = models.ImageField(
        _(u'Fotoğraf'),
        upload_to='photos/',
        null=True,
        blank=True)
    # video - not now of course
    date = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_(u'Gönderi zamanı'))
    # comments ??

    class Meta:
        verbose_name = _(u'gönderi')
        verbose_name_plural = _(u'gönderiler')


class UserPost(Post):
    owner = models.ForeignKey(CustomUser, related_name='posts')


class GroupPost(Post):
    writer = models.ForeignKey(CustomUser)

    owner = models.ForeignKey(Group, related_name='posts')
